﻿using System;

namespace INSS
{
    public class Inss : ICalculadorInss
    {
        public decimal CalcularDesconto(DateTime date, decimal salary)
        {
            var (INSSPercent, INSSFloor) = InssTable.GetData(salary, date);

            if (HasExceededFloor(INSSFloor)) return INSSFloor;

            return (salary * INSSPercent) / 100;
        }

        private bool HasExceededFloor(decimal floor) => floor > 0;
    }
}
