﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace INSS
{
    /// <summary>
    /// Tabela na camada de persistência (banco de dados)
    /// </summary>
    internal sealed class InssTable
    {
        /// <summary>
        /// Schema da tabela na camada de persistência (banco de dados)
        /// </summary>
        private struct Row
        {
            public int Percent { get; set; }
            public decimal Min { get; set; }
            public decimal Max { get; set; }
            public decimal Floor { get; set; }
            public DateTime EffectiveDate { get; set; }
        }
        /// <summary>
        /// Valores da camada de persistência (banco de dados)
        /// </summary>
        private readonly static IEnumerable<Row> Rows = new List<Row>
        {
            #region 2011
            new Row
            {
                Min = 0,
                Max = 1106.9M,
                Percent = 8,
                Floor = 0,
                EffectiveDate = new DateTime(2011, 01, 01)
            },
            new Row
            {
                Min = 1106.91M,
                Max = 1844.83M,
                Percent = 9,
                Floor = 0,
                EffectiveDate = new DateTime(2011, 01, 01)
            },
            new Row
            {
                Min = 1844.84M,
                Max = 3689.66M,
                Percent = 11,
                Floor = 0,
                EffectiveDate = new DateTime(2011, 01, 01)
            },
            new Row
            {
                Min = 3689.67M,
                Max = decimal.MaxValue,
                Floor = 405.86M,
                EffectiveDate = new DateTime(2011, 01, 01)
            },
            #endregion
            #region 2012
            new Row
            {
                Min = 0,
                Max = 1000,
                Percent = 7,
                Floor = 0,
                EffectiveDate = new DateTime(2012, 01, 01)
            },
            new Row
            {
                Min = 1000.01M,
                Max = 1500,
                Percent = 8,
                Floor = 0,
                EffectiveDate = new DateTime(2012, 01, 01)
            },
            new Row
            {
                Min = 1500.01M,
                Max = 3000,
                Percent = 9,
                Floor = 0,
                EffectiveDate = new DateTime(2012, 01, 01)
            },
            new Row
            {
                Min = 3000.01M,
                Max = 4000,
                Percent = 11,
                Floor = 0,
                EffectiveDate = new DateTime(2012, 01, 01)
            },
            new Row
            {
                Min = 4000.01M,
                Max = decimal.MaxValue,
                Floor = 500,
                EffectiveDate = new DateTime(2012, 01, 01)
            }
	        #endregion
        };
        /// <summary>
        /// Simula a query no banco
        /// </summary>
        /// <param name="salary">Salário</param>
        /// <param name="effectiveDate">Data de vigência</param>
        /// <returns></returns>
        public static (int Percent, decimal Floor) GetData(decimal salary, DateTime effectiveDate)
        {
            var data = Rows.FirstOrDefault(r => r.EffectiveDate.Year == effectiveDate.Year
                                                && salary >= r.Min
                                                && salary <= r.Max);
            return (data.Percent, data.Floor);
            //return Rows.Where(r => r.EffectiveDate.Year == effectiveDate.Year
            //                       && salary >= r.Min
            //                       && salary <= r.Max
            //                 ).Select(r => (r.Percent, r.Floor)).FirstOrDefault();
        }
    }
}
