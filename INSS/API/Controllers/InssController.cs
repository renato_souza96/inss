﻿using INSS;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InssController : ControllerBase
    {
        private readonly Inss _inss;

        public InssController(Inss inss)
        {
            _inss = inss;
        }

        [HttpGet("/calculate")]
        [SwaggerOperation(Summary = "Calculate the INSS discount", Description = "Returns the calculated INSS discount, given the effective date and salary")]
        public IActionResult Get([FromQuery] DateTime date, decimal salary)
        {
            if (date > DateTime.Now)
                return BadRequest("Date parameter must be less than the current date");

            var result = _inss.CalcularDesconto(date, salary);
            return Ok(result);
        }
    }
}
